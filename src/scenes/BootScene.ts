import 'phaser';

export default class BootScene extends Phaser.Scene
{
    constructor ()
    {
        super('BootScene');
    }
    init()
    {
        this.cameras.main.setBackgroundColor("#EFE25C");
    }
    create()
    {
        this.game.scene.start('LoadingScene');
    }
}
import 'phaser';

export default class GameScene extends Phaser.Scene
{
    constructor ()
    {
        super('GameScene');
    }
    init(args)
    {
        args = args || {};
        console.log('Game Scene Init');
    }
    preload()
    {
        this.cameras.main.setBackgroundColor("#7EBDEF");
    }
    create()
    {
    }
}
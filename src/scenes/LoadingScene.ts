import 'phaser';

export default class LoadingScene extends Phaser.Scene
{
    constructor ()
    {
        super('LoadingScene');
    }
    preload()
    {
        this.load.image('chinchilla_games_logo', 'assets/images/chinchilla_games_logo.png');
        this.cameras.main.setBackgroundColor("#CC7EEF");
    }
    create()
    {
        //this.game.scene.start('GameScene');
        //this.game.scene.start('MenuScene');
        this.game.scene.start('SplashScene');
    }
}
import 'phaser';

export default class MenuScene extends Phaser.Scene
{
    constructor ()
    {
        super('MenuScene');
    }
    preload()
    {
        this.cameras.main.setBackgroundColor("#DD6918");
    }
    create()
    {
        this.game.scene.start('GameScene')
    }
}
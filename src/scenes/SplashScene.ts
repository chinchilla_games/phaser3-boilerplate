import 'phaser';

export default class SplashScene extends Phaser.Scene
{
    constructor ()
    {
        super('SplashScene');
    }
    preload()
    {
        this.cameras.main.setBackgroundColor("#BC0470");
    }
    create()
    {
        this.add.image(
            this.cameras.main.centerX,
            this.cameras.main.centerY,
            'chinchilla_games_logo'
        ).setOrigin(0.5, 0.5);
        this.time.addEvent({
            delay: 2000,
            callback: () => this.game.scene.start('MenuScene')
        });
    }
}